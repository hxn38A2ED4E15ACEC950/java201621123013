package hou;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;  
  
import hou.Book;  
  
class User{
	private String name;
	private String password;
	public User(String name, String password) {
		this.name = name;
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
public class Mainclass {  
    /* 
    public static final int SIZE = 10; 
    Book[] booklist = new Book[SIZE]; 
    */  
    ArrayList booklist = new ArrayList();
    List list1 = new ArrayList();
    int count = 0;  
      
    public Mainclass()  
    {  
          
        Scanner scan = new Scanner(System.in);  
        String a = null;
        String b = null;
        System.out.println("请登录！！！");
        User user = new User(a,b);
        System.out.println("用户名：");
        a = scan.next();
        System.out.println("密码：");
        b = scan.next();
        System.out.println("恭喜你！登录成功~");
        printMenu();  
          
        while(scan.hasNext())  
        {  
            //读取用户输入  
           int choice = scan.nextInt();
              
            if(choice == 5)  
            {  
                System.out.println("成功退出系统，欢迎再次光临！");  
                break;  
            }  
            switch(choice)  
            {  
            case 1: addBook(); break; //添加图书 
            case 2: findBoo(); break; 
            case 3: printAllBook();break;
            case 4: show();break;
            default: System.out.println("输入非法"); printMenu(); continue;  
            } 
           
        }  
          
          
       
    }  
    
    
    void show() {
		for(int i=0;i<count;i++){
			
            Book book = (Book)list1.get(i);   
            
            System.out.println("第"+(i+1)+"本书名："+book.getBookname()+"书号为："+book.getBooknum());
		}
		
	}
	void printMenu()  
    {  
        //打印菜单  
        System.out.println("%n%n欢迎来JMU-啊侯的图书管理系统%n%n");  
        System.out.println("增加图书...1");  
        System.out.println("查找并借阅图书...2");
        System.out.println("展示所有图书信息...3");
        System.out.println("已借阅图书信息...4");
        System.out.println("退出系统...5");   
    }  
      
    void addBook()  
    {  
        if (count > booklist.size()-1)  
        {  
            System.out.println("当前共有:"+booklist.size()+"本书!");  
            Scanner scan = new Scanner(System.in);  
            System.out.println("请输入图书名：");  
            String bookname = scan.next();  
            System.out.println("请输入书号：");
            String booknum = scan.next();
            Book book = new Book(bookname,booknum);  
            //booklist[count] = book;  
            booklist.add(book); //ArrayList的增加的方法，是不是简便许多？  
            count++;  
            System.out.println("增加成功！");  
            printAllBook();  
        }  
        else  
        {  
            System.out.println("图书库已满！");  
        }
        printMenu();
    }  
      
    
      
    
      
    void findBoo()  
    {  
        Scanner scan = new Scanner(System.in);
        while(true)  
        {  
            System.out.println("请输入按哪种方法查找并借阅图书：1、书号/2、书名/3、返回主菜单");  
            int choose = scan.nextInt();  
            if(choose == 1)  
            {  
                System.out.println("请输入要查找图书的书号：");  
                String num = scan.next();  
                int id = numFind(num); 
                if(id > -1)  
                {  
                    Book book = (Book)booklist.get(id);//ArrayList获取指定元素的方法   
                    System.out.println("查找成功！查找的书名为："+book.getBookname()+"书号为："+book.getBooknum()); 
                    System.out.println("是否借阅此书籍？“是”请输入1，“否”请输入2 "); 
                    int number = scan.nextInt();
                    if(number==1){
                    	list1.add(book);
                    	System.out.println("借书成功！");
                    }
                    if(number==2){
                    	break;
                    }
                }
                
                else  
                {  
                    System.out.println("输入错误！");  
                }  
            }  
            else if(choose == 2)  
            {  
                System.out.println("请输入您要查找的书名：");  
                String name = scan.next();  
                int id = nameFind(name);  
                if(id > -1)  
                {  
                    Book book = (Book)booklist.get(id); 
                    System.out.println("查找成功，您查找到的书为第"+(id+1)+"本书！"+"书号为："+book.getBooknum());
                    System.out.println("是否借阅此书籍？“是”请输入1，“否”请输入2 "); 
                    int number = scan.nextInt();
                    if(number==1){
                    	list1.add(book);
                    	System.out.println("借书成功！");
                    }
                    if(number==2){
                    	break;
                    }
                }  
            }  
            else if(choose == 3)  
            {  
                printMenu();  
                break;  
            }  
            else  
            {  
                System.out.println("输入非法！");  
            }  
        }  
    }  
      
    void printAllBook()  
    {  
        for (int i = 0; i < count; i++)  
        {  
            Book book = (Book)booklist.get(i);   
            
            System.out.println("第"+(i+1)+"本书名："+book.getBookname()+"书号为："+book.getBooknum());  
        }  
    }  
      
    int orderFind(int number)  
    {  
        //System.out.println(number);  
        if(number <= count)  
        {  
            int id = number - 1;  
            return id;  
        }  
        else  
            return -1;  
    }  
      
    int nameFind(String name)  
    {  
        int id = -1;  
        for(int i = 0; i < count; i++)  
        {  
            Book book = (Book)booklist.get(i);  
            if(book.getBookname().equals(name))  
            {  
                id = i;  
                break;  
            }  
            else if(i<count)  
                continue;  
            else  
            {  
                System.out.println("未查找到您想要的书名");  
                break;  
            }  
        }  
        return id;  
    }  
    
    
    int numFind(String num)  
    {  
        int id = -1;  
        for(int i = 0; i < count; i++)  
        {  
            Book book = (Book)booklist.get(i);  
            if(book.getBooknum().equals(num))  
            {  
                id = i;  
                break;  
            }  
            else if(i<count)  
                continue;  
            else  
            {  
                System.out.println("未查找到您想要的书号");  
                break;  
            }  
        }  
        return id;  
    }  
      
    public static void main(String[] args) {  
        // TODO Auto-generated method stub  
        new Mainclass();  
    }  
  
}  