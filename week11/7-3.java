import java.util.Scanner;

public class Main{
	private static Scanner in;
	public static void main(String[] args) {
		in = new Scanner(System.in);
		int a=in.nextInt();
		double[] arr=new double[a];
		for (int i=0;i<a ;i++ ) {
			arr[i]=in.nextDouble();
		}
		int begin,end;
		while(true){
			try{
				begin=in.nextInt();
				end=in.nextInt();
			}
			catch(Exception e){
				break;
			}
			try{
				System.out.println(ArrayUtils.findMax(arr,begin,end));
			}
			catch (IllegalArgumentException e) {
				System.out.println(e);
			}
			
		}
		try {
     			System.out.println(ArrayUtils.class.getDeclaredMethod("findMax", double[].class,int.class,int.class));
		} 
		catch (Exception e1) {
		}
	}
}

class ArrayUtils{
	public ArrayUtils(){

	}
	public static double findMax(double[] arr,int begin, int end) throws IllegalArgumentException{
		if (begin>=end) {
			throw new IllegalArgumentException("begin:"+begin+" >= end:"+end);
		}
		if (begin<0) {
			throw new IllegalArgumentException("begin:"+begin+" < 0");
		}
		if (end>arr.length) {
			throw new IllegalArgumentException("end:"+end+" > arr.length");
		}
		double max=arr[0];
		for(int i=begin;i<end;i++){
			if(max<arr[i])
				max=arr[i];
		}
		return max;
	}
}
