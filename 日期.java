package hou;
import java.util.Scanner;

	import java.time.DayOfWeek;
	import java.time.Duration;
	import java.time.LocalDate;
	import java.time.LocalDateTime;
	import java.time.LocalTime;
	import java.time.OffsetDateTime;
	import java.time.ZoneId;
	import java.time.ZoneOffset;
	import java.time.ZonedDateTime;
	import java.time.format.DateTimeFormatter;

	public class main {

		public static boolean isMondayToFriday(){
			LocalDateTime now = LocalDateTime.now();
			DayOfWeek dayOfWeek = now.getDayOfWeek();
			System.out.println(dayOfWeek);
			if(dayOfWeek!=DayOfWeek.SATURDAY && dayOfWeek!=DayOfWeek.SUNDAY){
				System.out.println("工作日侯湘宁201621123013");
				return true;
			}
			else{
				System.out.println("休息侯湘宁201621123013");
				return false;
			}
			
		}

		public static void main(String[] args) {
			LocalTime localTime = LocalTime.of(0, 0,0);//无时区概念
			System.out.println(localTime);
			LocalDate localDate = LocalDate.of(1998, 10, 31);
			System.out.println(localDate);
			ZonedDateTime zonedDateTime = ZonedDateTime.of(localDate,localTime,ZoneId.of("Asia/Shanghai"));//有时区概念
			System.out.println(zonedDateTime);
			
			System.out.println(zonedDateTime.toEpochSecond());////机器观点当前秒数
			System.out.println(zonedDateTime.toInstant());
			System.out.println(zonedDateTime.toInstant().toEpochMilli());//机器观点当前毫秒数
			
			System.out.println(LocalTime.now());//当前时间，当前系统默认时区
			System.out.println(LocalDate.now());//当前日期，当前系统默认时区
			System.out.println(LocalDateTime.now());//当前日期与时间，当前系统默认时区
			LocalDate nowDate = LocalDate.now();
			LocalTime nowTime = LocalTime.now();
			System.out.println(OffsetDateTime.of(nowDate, nowTime, ZoneOffset.UTC));//
			System.out.println(OffsetDateTime.now().toEpochSecond());
			
			System.out.println("Year, YearMonth, Month, MonthDay");
			
			System.out.println("TemporalAmount");
			String pattern = "E MM/dd/yyyy";
			System.out.println(LocalDate.of(1989, 9, 22).plusDays(5).plusMonths(6).plusWeeks(3).format(DateTimeFormatter.ofPattern(pattern)));
			
			System.out.println(isMondayToFriday());
			Duration thirtyDay = Duration.ofDays(30);
	        System.out.println(thirtyDay.toString());
			
		}
	}