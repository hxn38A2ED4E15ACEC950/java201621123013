import java.util.Scanner;

class Person {
	private String name;
	private int age;
	private boolean gender;
	private int id;
	
	public Person(){
		System.out.println("This is constructor");
		System.out.printf("%s,%d,%s,%d%n", name,age,gender,id);
	}

	public Person(String name, int age, boolean gender, int id) {
		super();
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public boolean isGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	public String toString() {
		return "Person [name=" + name + ", age=" + age + ", gender=" + gender + ", id=" + id + "]";
	}
	
}
public class Main{
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in 

);
		int n = Integer.parseInt(in.nextLine());
		Person[] persons = new Person[n];
		for (int j = 0; j < persons.length; j++) {
			Person person = new Person(in.next(),in.nextInt(),in.nextBoolean(), 0);
			persons[j] = person;
		}
		for (int i = persons.length-1; i >=0; i--) {
			System.out.println(persons[i]);
		}
		System.out.println(new Person());
		in.close();
	}
}