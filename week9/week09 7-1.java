import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

  interface IntegerStack {

	public Integer push(Integer item); 
	public Integer pop();              
	public Integer peek();             
	public boolean empty();           
	public int size();         
}

  class ArrayListIntegerStack implements IntegerStack{

	List<Integer>arraylist = new ArrayList<Integer>();
	@Override
	public Integer push(Integer item) {
		if(item==null)
		return null;
		arraylist.add(item);
		return item;
	}

	@Override
	public Integer pop() {
		// TODO Auto-generated method stub
		if(arraylist.size()==0)
		return null;
		return arraylist.remove(arraylist.size()-1);
	}

	@Override
	public Integer peek() {
		// TODO Auto-generated method stub
		if(arraylist.size()==0)
			return null;
		return arraylist.get(arraylist.size()-1);
	}

	
	@Override
	public boolean empty() {
		// TODO Auto-generated method stub
		return arraylist.size()==0?true:false;
	}


	@Override
	public int size() {
		// TODO Auto-generated method stub
		return arraylist.size();
	}

	public String toString(){
		return arraylist.toString();
	}
  }
  public class Main{
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		ArrayListIntegerStack stack = new ArrayListIntegerStack();
		int m = sc.nextInt();
		for(int i=0;i<m;i++){
			int a = sc.nextInt();
			stack.push(a);
			System.out.println(a);
		}
		System.out.println(stack.peek()+","+stack.empty()+","+stack.size());
		System.out.println(stack.toString());
		String x = sc.next();
		int y = Integer.parseInt(x);
		for(int i=0;i<y;i++){
			System.out.println(stack.pop());
		}
		System.out.println(stack.peek()+","+stack.empty()+","+stack.size());
		System.out.println(stack.toString());
	}
}