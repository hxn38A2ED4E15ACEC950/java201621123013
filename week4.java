import java.util.Arrays;
import java.util.Scanner;

abstract class Shape{
	public final static double PI = 3.14;
	public abstract double getArea();
	public abstract double getPerimeter();
	
}

class Rectangle extends Shape{
	private int width;
	private int length;
	
	public Rectangle(int width,int length){
		this.width = width;
		this.length = length;
	}
	
	public double getPerimeter(){
		return 2*(width+length);
	}
	
	public double getArea(){
		return width*length;
	}
	
	
	@Override
	public String toString() {
		return "Rectangle [width=" + width + ", length=" + length + "]";
	}
}

class Circle extends Shape{
	private int radius;
	
	public Circle(int radius){
		this.radius = radius;
	}
	
	public double getArea(){
		return Shape.PI*radius*radius;
	}

	

	@Override
	public double getPerimeter() {
		return  2*Shape.PI*radius;
	}
	
	@Override
	public String toString() {
		return "Circle [radius=" + radius + "]";
	}
}


public class Main {
	public static double sumAllArea(Shape[] shapes){
		double sum = 0;
		for(Shape e:shapes){
			sum += e.getArea();
		}
		return sum;
	}
	public static double sumAllPermeter(Shape[] shapes){
		double sum = 0;
		for(Shape e:shapes){
			sum += e.getPerimeter();
		}
		return sum;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in 

);
		int n = Integer.parseInt(in.nextLine());
		Shape[] shapes = new Shape[n];
		for (int i = 0;i < shapes.length;i++){
			String line = in.nextLine();
			if(line.equals("rect")){
				String[] num = in.nextLine().split(" ");
				Rectangle rect = new Rectangle(Integer.parseInt(num[0]),Integer.parseInt(num[1]));
				shapes[i] = rect;
			}
			else{
				Circle cir = new Circle(Integer.parseInt(in.nextLine()));
				shapes[i] = cir;
			}
		}
		System.out.println(sumAllPermeter(shapes));
		System.out.println(sumAllArea(shapes));
		System.out.println(Arrays.toString(shapes));
		for(Shape shape : shapes){
			System.out.printf("%s,%s\n",shape.getClass(),shape.getClass().getSuperclass());
		}
	}

}
