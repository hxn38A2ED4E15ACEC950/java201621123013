package hou;

import java.util.Scanner;
import  java.util.Arrays;

class PersonSortable implements Comparable <PersonSortable> {
    private String name;
    private  int age;

    public PersonSortable(String name, int age) {
        this.name = name;
        this.age = age;
    }
    @Override
    public String toString() {
        return name+"-"+age;
    }

    @Override
    public int compareTo(PersonSortable other) {
        if (this.name .compareTo(other.name ) > 0 )
        {
            return 1;
        }
        else if (this.name.compareTo(other.name ) < 0)
        {
            return -1;
        }
        else {
            if (this.age > other.age)
            {
                return 1;
            }
            else if (this.age < other.age)
            {
                return -1;
            }
            else
                return 0;
        }
    }
}
public class Main {
    public static void main(String[] args) {
    	
        Scanner sc = new Scanner(System.in );
        
        int n = sc.nextInt();
        PersonSortable [] personSortable = new PersonSortable[n];
        for (int i = 0; i < n; i++) {
            personSortable[i] = new PersonSortable(sc.next(),sc.nextInt());
        }
        Arrays.sort(personSortable);
        for (int i = 0; i < n; i++) {
            System.out.println(personSortable[i]);

        }
        System.out.println(Arrays.toString(PersonSortable.class.getInterfaces()));
    }
}