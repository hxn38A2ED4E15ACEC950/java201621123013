import java.util.Scanner;
class Person{
	private String name;
	private boolean gender;
	private int age;
	private int id;
	private static int count;
	
	public Person(){
		System.out.println("This is constructor");
		System.out.printf("%s,%d,%s,%d%n",name,age,gender,id);
	}
	
	public Person(String name,int age, boolean gender){
		this.name =name;
		this.gender=gender;
		this.age=age;
		//this.id=id;
	}
	public String toString(){
		return "Person [name=" + name + ", age=" + age + ", gender=" + gender + ", id=" + id + "]";
	}
	
	static{
		System.out.println("This is static initialization block");
	}
	
	{
		 System.out.println("This is initialization block, id is "+count);
		 id = count;
		 count++;
	}
}

public class Main{
	public static void main(String[] args) {
	  
		Scanner in = new Scanner(System.in );
		
		int n = Integer.parseInt(in.nextLine());
		
		Person[] persons = new Person[n];
		for(int i=0;i<persons.length;i++){
			Person person = new Person(in.next(),in.nextInt(),in.nextBoolean());
			persons[i]=person;
		}
		for (int j = persons.length-1; j >=0; j--) {
			System.out.println(persons[j]);
		}
		System.out.println(new Person());
		in.close();
	}
}