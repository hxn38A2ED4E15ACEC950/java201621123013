class Account{
	private int balance;

	
	public Account(int balance) {
		super();
		this.balance = balance;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	void deposit(int money){
		balance = balance + money;
	}
	
	void withdraw(int money) {
		balance = balance - money;
	}
	
}
