 class MonitorTask implements Runnable{

	private volatile boolean flag = false;  
	private String word;
	@Override
	public void run() {
		while(!flag){
			if(word!=null){
				if(word.contains("alien"))
					System.out.println(Thread.currentThread().getName()+" found alien in "+word);
				this.word=null;
			}
		}
		System.out.println(Thread.currentThread().getName() + " stop");	
	}
	
	public void stopMe(){
		flag=true;
	}
	public void sendWord(String word) {
		this.word=word;
	}
	
}
