import java.util.Scanner;

public class Main{

    public static void main(String[] args) {
   
        System.out.println(ArrayUtils.findMinMax(null));
        System.out.println(ArrayUtils.PairResult.class);
    }

}
class ArrayUtils{
   

    public static PairResult findMinMax(double[] values){
        Scanner sc=new Scanner(System.in);
        int n;
        n=sc.nextInt();
        Double [] arr=new Double[n];
        for(int i=0;i<n;i++){
            arr[i]=sc.nextDouble();
        }
        PairResult p=new PairResult(arr[0], arr[0]);
        for(int i=1;i<arr.length;++i){
            if(arr[i]>p.getMax())
               p.setMax(arr[i]);
            else
                if(arr[i]<p.getMin())
                    p.setMin(arr[i]);
        }
        return p;
       
    }
   
    public static class PairResult{
        private double min;
        private double max;
       
       
        public double getMin() {
            return min;
        }


        public void setMin(double min) {
            this.min = min;
        }


        public double getMax() {
            return max;
        }


        public void setMax(double max) {
            this.max = max;
        }


        public PairResult(double min, double max) {
            super();
            this.min = min;
            this.max = max;
        }


        @Override
        public String toString() {
            return "PairResult [min=" + min + ", max=" + max + "]";
        }
       
    }
}
