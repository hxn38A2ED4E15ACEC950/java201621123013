import java.util.Arrays;
import java.util.Scanner;

public class Main{

    private static Scanner in;

    public static void main(String[] args) {
        in = new Scanner(System.in);
        int n;
        Integer a;
        while (true) {
            n = in.nextInt();
            ArrayIntegerStack arrayIntegerStack = new ArrayIntegerStack(n);
            n = in.nextInt();
            for (int i = 0; i < n; i++) {
                a = in.nextInt();
                a= arrayIntegerStack.push(a);
                System.out.println(a);
            }
            System.out.println(arrayIntegerStack.peek() + "," + arrayIntegerStack.empty() + "," + arrayIntegerStack.size());
            System.out.println(Arrays.toString(arrayIntegerStack.getIntegers()));
            n = in.nextInt();
            for (int i = 0; i < n; i++) {
                System.out.println(arrayIntegerStack.pop());
            }
            System.out.println(arrayIntegerStack.peek() + "," + arrayIntegerStack.empty() + "," + arrayIntegerStack.size());
            System.out.println(Arrays.toString(arrayIntegerStack.getIntegers()));
        }
    }

}

interface IntegerStack {

    public Integer push(Integer item);

    public Integer pop();

    public Integer peek();

    public boolean empty();

    public int size();
}

class ArrayIntegerStack implements IntegerStack {

    private final Integer[] integers;
    private int length;
    private final int n;

    public ArrayIntegerStack(int n) {
        integers = new Integer[n];
        length = 0;
        this.n = n;
    }

    @Override
    public Integer push(Integer item) {
        if (item == null) {
            return null;
        }
        if (length >= n) {
            return null;
        }
        integers[length] = item;
        length++;
        return item;
    }

    @Override
    public Integer pop() {
        if (length == 0) {
            return null;
        }
        Integer a;
        a = integers[length - 1];
        length--;
        return a;
    }

    @Override
    public Integer peek() {
        if (length == 0) {
            return null;
        }
        return integers[length - 1];
    }

    @Override
    public boolean empty() {
        return length == 0;
    }

    @Override
    public int size() {
        return length;
    }

    public Integer[] getIntegers() {
        return integers;
    }
}
