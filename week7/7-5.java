import java.text.DecimalFormat;
import java.util.*;

public class Main {
    private static Scanner in;
    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        in=new Scanner(System.in);
        String name,stuNo,clazz,T="s";
        int age,d=0;
        boolean gender;
        Company company;
        double salary;
        List<Person> peopleList=new ArrayList<Person>();
        while (true){
            while (T.equals("e")||T.equals("s")){
                T=in.next();
                if (T.equals("s")){
                    name=in.next();
                    age=in.nextInt();
                    gender=in.nextBoolean();
                    stuNo=in.next();
                    clazz=in.next();
                    if (name.equals("null")) name=null;
                    if (stuNo.equals("null"))stuNo=null;
                    if (clazz.equals("null")) clazz=null;
                    peopleList.add(new Student(name,age,gender,stuNo,clazz));

                } else if (T.equals("e")){
                    name=in.next();
                    age=in.nextInt();
                    gender=in.nextBoolean();
                    salary=in.nextDouble();
                    company=new Company(in.next());
                    if (name.equals("null")) name=null;
                    if (company.getName().equals("null"))company=null;
                    peopleList.add(new Employee(name,age,gender,salary,company));
                }

            }



            String exit=in.next();
            if (exit.equals("exit")||exit.equals("return"))
                break;
            Collections.sort(peopleList);
            for (Person e:peopleList)
            {
                if (e instanceof Student)
                    System.out.println((Student)e);
                if (e instanceof Employee)
                    System.out.println((Employee)e);

            }


            ArrayList<Student> stuList=new ArrayList<Student>();
            ArrayList<Employee> empList=new ArrayList<Employee>();
            for (Person e:peopleList) {
                if (e instanceof Employee){
                    d=0;
                     for (Person k:empList) {
                        if (k.equals(e))
                            d=1;
                    }
                    if(d==0)
                        empList.add((Employee)e);

                }


                else{
                    d=0;
                    for (Person k:stuList) {
                        if (k.equals(e))
                            d=1;
                    }
                    if(d==0)stuList.add((Student) e);
                }

            }

            System.out.println("stuList");
            for (Student e:stuList) {
                System.out.println(e);
            }

            System.out.println("empList");
           for (Employee e:empList) {
                System.out.println(e);
            }

        }


    }
}

 @SuppressWarnings("rawtypes")
class Person implements Comparable{
    private String name;
    private int age;
    private boolean gender;

    public Person() {
    }

    public Person(String name, int age, boolean gender)
    {
        this.name = name;
        this.age = age;
        this.gender=gender;
    }

    @Override
    public String toString() {
        return name + "-" + age + "-" + gender ;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Person)) return false;

        Person person = (Person) obj;

        if (age != person.age) return false;
        if (gender != person.gender) return false;
        return name != null ? name.equals(person.name) : person.name == null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

     public int compareTo(Object obj) {
         Person person=(Person) obj;
         if (this.name.compareTo(person.name)>0) return 1;
         if (this.name.compareTo(person.name)<0) return -1;
         if (this.age>person.age) return 1;
         if (this.age<person.age) return -1;
         return 0;
     }
 }


class Student extends Person{

   private String stuNo;
   private String clazz;

    public Student(String name, int age, boolean gender, String stuNo, String clazz) {
        super(name, age, gender);
        this.stuNo=stuNo;
        this.clazz=clazz;
    }

    @Override
    public String toString(){
        return "Student:"+super.toString()+"-"+stuNo+"-"+clazz;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Student)) return false;
        if (!super.equals(obj)) return false;

        Student student = (Student) obj;

        if (stuNo != null ? !stuNo.equals(student.stuNo) : student.stuNo != null) return false;
        return clazz != null ? clazz.equals(student.clazz) : student.clazz == null;
    }


    public String getStuNo() {
        return stuNo;
    }

    public void setStuNo(String stuNo) {
        this.stuNo = stuNo;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }
}


class Company {
    private String name;
    public Company(String name) {
        super();
        this.name=name;
    }

    @Override
    public String toString() {
        return name ;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Company)) return false;

        Company company = (Company) obj;

        return name != null ? name.equals(company.name) : company.name == null;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }
}

class Employee extends Person{
    private Company company;
    private double salary;

    public Employee(String name, int age, boolean gender, double salary, Company company){
        super(name, age,gender);
        this.salary=salary;
        this.company=company;
    }
    @Override
    public String toString(){
        if (company==null)
            return "Employee:"+super.toString()+"-"+"null"+"-"+salary;
        else return "Employee:"+super.toString()+"-"+company.toString()+"-"+salary;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Employee)) return false;
        if (!super.equals(obj)) return false;

        Employee employee = (Employee) obj;
        DecimalFormat df1 = new DecimalFormat("#.#");
        DecimalFormat df2 = new DecimalFormat("#.#");

        if (!df1.format(employee.salary).equals(df2.format(salary))) return false;
        return company != null ? company.equals(employee.company) : employee.company == null;
    }


    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
}
