import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;

public class Main {
     public static void main(String[] args) {
        
    	 Scanner sc = new Scanner(System.in);
    	    Map<String,Integer> dict = new TreeMap<String,Integer>();
    		while(sc.hasNext()){
    			String words = sc.nextLine();
    			if(words.equals("!!!!!"))
    				break;
    		Scanner linescanner = new Scanner(words);
    		while(linescanner.hasNext()){
    			String word = linescanner.next();
    			if(dict.containsKey(word))
    				dict.put(word, dict.get(word)+1);
    			else
    				dict.put(word, 1);
    		}
    		linescanner.close();
  }
    		
    		System.out.println(dict.size());
    		
    		List<Entry<String,Integer>> list = new ArrayList<Entry<String,Integer>>(dict.entrySet());
    		dict.entrySet();
    		Collections.sort(list,new Comparator<Entry<String,Integer>>() {
    			
				@Override
				public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
					// TODO Auto-generated method stub
					if(o2.getValue()-o1.getValue()!=0){
    			      return o2.getValue()-o1.getValue();
					}
					return o1.getKey().compareTo(o2.getKey());
				}
    			
			});
    		for(int i=0;i<list.size();i++){
    			if(i==10)
    				break;
    			Entry<String,Integer> entry=list.get(i);
    			System.out.println(entry.getKey()+"="+entry.getValue());
    		}
}
}