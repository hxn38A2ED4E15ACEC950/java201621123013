
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

interface GeneralStack<E>{
    E push(E item);
    E pop();
    E peek();
    public boolean empty();
    public int size();
}

class Car{
    private int id;
    private String name;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id 

 = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    @Override
    public String toString() {
        return "Car [id=" + id + ", name=" + name + "]";
    }
    public Car(int id, String name) {
        super();
        this.id = id;
        this.name = name;
    }

}


class ArrayListGeneralStack<E> implements GeneralStack<E> {

    List<E> list = new ArrayList<E>();

    @Override

    public E push(Object item) {
        if (item == null)
            return null;
        list.add((E) item);
        return (E) item;
    }

    @Override
    public E pop() {
        if (list.size() == 0)
            return null;
        return list.remove(list.size() - 1);
    }

    @Override
    public E peek() {
        if (list.size() == 0)
            return null;
        return list.get(list.size() - 1);
    }

    @Override
    public boolean empty() {
        return list.size() == 0 ? true : false;
    }

    @Override
    public int size() {
        return list.size();
    }

    public String toString() {
        return list.toString();
    }

    public List<E> getArrayList() {
        return list;
    }

}

public class Main{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in 

);
        while(true){
            String choice = sc.next();
            ArrayListGeneralStack<?> stack;
            switch(choice){
                case"quit":
                    System.exit(0);

                case"Integer":
                    System.out.println("Integer Test");
                    int m = sc.nextInt();
                    int n = sc.nextInt();
                    stack = new ArrayListGeneralStack<Integer>();
                    int sum = 0;
                    for(int i=0;i<m;i++){
                        int a = sc.nextInt();
                        System.out.println("push:"+stack.push(a));
                    }

                    for(int i=0;i<n;i++){
                        System.out.println("pop:"+stack.pop());
                    }

                    System.out.println(stack);

                    for(int i=0;i<stack.size();i++){
                        Integer b = (Integer) stack.getArrayList().get(i);
                        sum+=b;
                    }
                    System.out.println("sum="+sum);
                    System.out.println(stack.getClass().getInterfaces()[0]);
                    break;
                case"Double":
                    System.out.println("Double Test");
                    double sum1 = 0.0;
                    double m1 = sc.nextDouble();
                    double n1 = sc.nextDouble();
                    stack = new ArrayListGeneralStack<Integer>();

                    for(int i=0;i<m1;i++){
                        double a = sc.nextDouble();
                        System.out.println("push:"+stack.push(a));
                    }

                    for(int i=0;i<n1;i++){
                        System.out.println("pop:"+stack.pop());
                    }
                    System.out.println(stack);

                    for(int i=0;i<stack.size();i++){
                        double b = (Double) stack.getArrayList().get(i);
                        sum1+=b;
                    }
                    System.out.println("sum="+sum1);
                    System.out.println(stack.getClass().getInterfaces()[0]);
                    break;
                case"Car":
                    System.out.println("Car Test");

                    int m2 = sc.nextInt();
                    int n2 = sc.nextInt();

                    stack = new ArrayListGeneralStack<Car>();

                    for (int i=0;i<m2;i++) {

                        System.out.println("push:" + stack.push(new Car(sc.nextInt(),sc.next())));
                    }

                    for (int i=0;i<n2;i++) {
                        System.out.println("pop:" + stack.pop());
                    }
                    if(n2>=m2){
                        System.out.println(stack);
                        System.out.println("null");
                    }
                    else {
                        System.out.println(stack);
                        for (int i = stack.size()-1; i >=0; i--){
                            System.out.println(((Car)stack.getArrayList().get(i)).getName());
                        }
                    }
                    System.out.println(stack.getClass().getInterfaces()[0]);
                    break;

                default:
                    break;
            }
        }
    }
}

