import java.util.Scanner;
import java.util.Map;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.TreeSet;

public class Main{

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        ArrayList<String> list0 = new ArrayList<>();
        ArrayList<String> list1 = new ArrayList<>();
        ArrayList<String> list2 = new ArrayList<>();

        String sc = s.nextLine();
        while (sc.compareTo("!!!!!") != 0) {
            list0.add(sc);
            String a[] = sc.split(" ");
            for (int i = 0; i < a.length; i++) {
                list1.add(a[i]);//
            }
            sc = s.nextLine();
        }
        TreeSet<String> set = new TreeSet<String>(list1);
        
        list1.clear();
        
        list1.addAll(set);
        
        Collections.sort(list1, (String o1, String o2) -> {
            return o1.compareTo(o2);
        });

        for (int i = 0; i < list1.size(); i++) {
            String k = list1.get(i) + "=" + "[";
            for (int j = 0; j < list0.size(); j++) {
                ArrayList<String> list5 = new ArrayList<>();
                int flag = 0;
                String a[] = list0.get(j).split(" ");
                for (int g = 0; g < a.length; g++) {
                    list5.add(a[g]);
                }
                for (int d = 0; d < list5.size(); d++) {
                    if (list1.get(i).compareTo(list5.get(d)) == 0) {
                        flag = 1;
                    } else {
                        if (flag != 1) {
                            flag = 0;
                        }
                    }
                }
                if (flag == 1) {
                    if (k.compareTo(list1.get(i) + "=" + "[") == 0) {
                        k = k + (j + 1);
                    } 
                    else {
                        k = k + ", " + (j + 1);
                    }
                }
            }
            k = k + "]";
            list2.add(k);
        }
        for (int i = 0; i < list2.size(); i++) {
            System.out.println(list2.get(i));
        }

        while (s.hasNextLine()) {
            ArrayList<Integer> list3 = new ArrayList<>();
            sc = s.nextLine();
            String a[] = sc.split(" ");
            for (int i = 0; i < list0.size(); i++) {
                ArrayList<String> list5 = new ArrayList<>();
                int c = 0;
                int flag = 1;
                String p[] = list0.get(i).split(" ");
                for (int g = 0; g < p.length; g++) {
                    list5.add(p[g]);
                }
                for (int j = 0; j < a.length; j++) {
                    for (int d = 0; d < list5.size(); d++) {
                        if (a[j].compareTo(list5.get(d)) == 0) {
                            c = 1;
                        } else {
                            if (c != 1) {
                                c = 0;
                            }
                        }
                    }
                    if (flag == 1 && c == 1) {
                        flag = 1;
                    } else {
                        flag = 0;
                    }
                    c = 0;
                }

                if (flag == 1) {
                    list3.add(i + 1);
                }
            }
            if (!list3.isEmpty()) {
                String k = "[";
                for (int i = 0; i < list3.size(); i++) {
                    if (k.compareTo("[") == 0) {
                        k = k + list3.get(i);
                    } else {
                        k = k + ", " + list3.get(i);
                    }
                }
                k = k + "]";
                System.out.println(k);
                for (int i = 0; i < list3.size(); i++) {
                    System.out.println("line " + list3.get(i) + ":" + list0.get(list3.get(i) - 1));
                }
            }
            else {
                System.out.println("found 0 results");
            }
        }

    }
}
